function editSectionFail(data, error, param){
  console.log("Error edit section");
  console.log(data.responseText);
  console.log(error);
  console.log(param);
}
function addNewSectionSuccess(data){
  location.hash = "#!/sections/"+data.id;
}
function addNewSectionFail(data){
  console.log("Error add section");
  console.log(data);
}
function addNewSection(){
  var newSectionId = $('#newSectionId').val();
  var newSectionName = $('#newSectionName').val();
  var newSectionCapacity = $('#newSectionCapacity').val();
  var newSectionType = $('#newSectionType').val();
  var request = {id:newSectionId, name:newSectionName, capacity:newSectionCapacity, type:newSectionType};

  $.ajax({
    url: 'api/sections.php',
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: "json",
    data: JSON.stringify(request),
    success: addNewSectionSuccess,
    error: addNewSectionFail
  });

}
function editSectionQuery(callback){
  var sectionId = $('#sectionId').val();
  var sectionName = $('#sectionName').val();
  var sectionCapacity = $('#sectionCapacity').val();
  var sectionType = $('#sectionType').val();
  var request = {id:sectionId, name:sectionName, capacity:sectionCapacity, type:sectionType};
  console.log(request);
  $.ajax({
    url: 'api/sections.php',
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: "json",
    data: JSON.stringify(request),
    success: callback,
    error: editSectionFail
  });

}

function updateSectionsFail(data, error){
  console.log("callback fail");
  console.log(data);
  console.log(error);
}
function updateSectionsQuery(callback){
  $.ajax({
    url: 'api/sections.php',
    type: 'GET',
    success: callback,
    error: updateSectionsFail
  });
}
