function get_type_name(id){
  if (id === "0" || id === 0){
    return "Секция";
  }else if (id === "1" || id === 1){
    return "Мастер-класс";
  }else{
    return id;
  }
}
function updateRequestsFail(data, error){
  console.log("callback fail");
  console.log(data);
  console.log(error);
}
function updateRequestsQuery(callback){
  $.ajax({
    url: 'api/requests.php',
    type: 'GET',
    success: function(data){
      for(var i in data){
        data[i].approved = parseInt(data[i].approved);
        data[i].sectionId= parseInt(data[i].sectionId);
        data[i].id= parseInt(data[i].id);
        data[i].section.type_name = get_type_name(data[i].section.type);
      }
      console.log(data);
      callback(data);
    },
    error: updateRequestsFail
  });
}
function addNewRequestSuccess(data){
  location.hash = "#!/requests/"+data.id;
}
function addNewRequestFail(data){
  console.log("Error add Request");
  console.log(data);
}
function addNewRequest(){
  var newRequestSectionId = $('#sectionId').val();
  var newRequestEmail= $('#email').val();
  var newRequestFirstName = $('#firstName').val();
  var newRequestLastName = $('#lastName').val();
  var newRequestTheme = $('#theme').val();
  var request = {sectionId:newRequestSectionId, email:newRequestEmail, firstName: newRequestFirstName, lastName: newRequestLastName, theme: newRequestTheme};

  $.ajax({
    url: 'api/requests.php',
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: "json",
    data: JSON.stringify(request),
    success: addNewRequestSuccess,
    error: addNewRequestFail
  });

}
function editRequestQuery(callback){
  var requestId = $('#requestId').val();
  var sectionId = $('#sectionId').val();
  var firstName = $('#firstName').val();
  var lastName = $('#lastName').val();
  var email = $('#email').val();
  var theme = $('#theme').val();
  var request = {id:requestId, sectionId:sectionId, email:email, firstName:firstName, lastName:lastName, theme:theme};

  $.ajax({
    url: 'api/requests.php',
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: "json",
    data: JSON.stringify(request),
    success: callback,
    error: editRequestsFail
  });

}

function editRequestsFail(data, error){
  console.log("callback fail");
  console.log(data);
  console.log(error);
}
