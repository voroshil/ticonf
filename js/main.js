function displayLeftSide(requests, approved){
  $('#requestsAmount').text(requests);
  $('#approvedAmount').text(approved);
}


function updateSectionsCombo(data){
  var template = $('#registerFormSectionsTemplate').html();
  Mustache.parse(template);  
  var rendered = Mustache.render(template, {sections: data});
  $("#sectionId").html(rendered);
}
function updateSectionsLeftSide(data){
  var template = $('#sectionsPageLeftTemplate').html();
  Mustache.parse(template);  
  var rendered = Mustache.render(template, {sections: data});
  $("#leftgutter").html(rendered);
}
function updateSectionForm(data){
  var template = $('#sectionsEditSectionTemplate').html();
  Mustache.parse(template);  
  console.log(data);
  var rendered = Mustache.render(template, data);
  $("#center").html(rendered);
}

function updateRequestsSuccess(data){
  var requests = 0;
  var approved = 0;
  for (var idx in data){
    if (parseInt(data[idx].approved) === 1){
      approved += 1;
    } 
    requests += 1;
  }
 displayLeftSide(requests, approved);
}

function registerPagePostRender(){
  updateSectionsQuery(updateSectionsCombo);
  updateRequestsQuery(updateRequestsSuccess);
}
function sectionsPagePostRender(){
  var hash = location.hash;
  if (!hash.endsWith('/')){
    hash += '/';
  }
  var id = '';
  if (hash.startsWith("#!/sections/")){
    id = hash.replace(/^#!\/sections\/(\d+)\//,'$1');
  }

  updateSectionsQuery(function(data){
    updateSectionsLeftSide(data);
    for(var  i in data){ 
      if (data[i].id === id){
        updateSectionForm(data[i]);
        break;
      }
    }
  });
}
function editSection(){
  editSectionQuery(sectionsPagePostRender);
}
function renderPage(mainTemplate, leftTemplate, afterRenderCallback){
  var template = $(mainTemplate).html();
  if (template !== undefined){
    Mustache.parse(template);  
    var rendered = Mustache.render(template, {});
    $("#center").html(rendered);
  }else{
    $("#center").html('');
  }
  var template = $(leftTemplate).html();
  if (template !== undefined){
    Mustache.parse(template);  
    var rendered = Mustache.render(template, {});
    $("#leftgutter").html(rendered);
  }else{
    $("#leftgutter").html('');
  }
  if (afterRenderCallback !== undefined){
    afterRenderCallback();
  }
}
function updateRequestsCenter(data){
  var template = $('#requestsListRequestTemplate').html();
  Mustache.parse(template);  
  var rendered = Mustache.render(template, {requests: data});
  $("#center").html(rendered);
}

function updateRequestsForm(data){
  var template = $('#requestsEditRequestTemplate').html();
  Mustache.parse(template);  
  var rendered = Mustache.render(template, data);
  $("#center").html(rendered);
}
function requestsPagePostRender(){
  var hash = location.hash;
  if (!hash.endsWith('/')){
    hash += '/';
  }
  var id = '';
  if (hash.startsWith("#!/requests/")){
    if (hash.match(/^#!\/requests\/(\d+)\//)){
      id = parseInt(hash.replace(/^#!\/requests\/(\d+)\//,'$1'));
    }
  }
  console.log(id);
  updateRequestsQuery(function(data){
      updateRequestsSuccess(data);
    if (id === ''){
      updateRequestsCenter(data);
    }
    for(var  i in data){ 
      if (data[i].id === id){
        updateRequestsForm(data[i]);
        break;
      }
    }
  });
}
function editRequest(){
  editRequestQuery(requestsPagePostRender);
}

var routes = [
  {prefix:/^#!\/register\//, mainTemplate:'#registerPageMainTemplate', leftTemplate:'#registerPageLeftTemplate', postAction:registerPagePostRender},
  {prefix:/^#!\/sections\/addnew\//, mainTemplate:'#sectionsAddSectionTemplate', leftTemplate:'#sectionsPageLeftTemplate', postAction:undefined},
  {prefix:/^#!\/sections\/$/, mainTemplate:undefined, leftTemplate:'#sectionsPageLeftTemplate', postAction:sectionsPagePostRender},
  {prefix:/^#!\/sections\//, mainTemplate:'#sectionsEditSectionTemplate', leftTemplate:'#sectionsPageLeftTemplate', postAction:sectionsPagePostRender},
  {prefix:/^#!\/requests\/$/, mainTemplate:"#requestsListRequestTemplate", leftTemplate:'#registerPageLeftTemplate', postAction:requestsPagePostRender},
  {prefix:/^#!\/requests\//, mainTemplate:'#requestsEditRequestTemplate', leftTemplate:'#requestsPageLeftTemplate', postAction:requestsPagePostRender},
];

function updatePage(){
  var hash = location.hash;
  if (!hash.endsWith('/')){
    hash += '/';
  }
  for(var i in routes){
    var route = routes[i];
 
    if (hash.match(route.prefix)){
      renderPage(route.mainTemplate, route.leftTemplate,route.postAction);
      return;
    }
  }
  location.hash = "#!/register";
}
function onloadRoutine(){
  $(window).bind( 'hashchange',updatePage);
  updatePage();
}
