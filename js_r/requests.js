import React from 'react';
import Api from 'api';

import {CheckBox, Button, TextInput, EmailInput} from 'app';

class RequestsList extends React.Component {
  constructor(props){
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }  
  tablerow(r, f){
    if(this.props.profile.isAuthenticated){
    const url = "#!/requests/" + r.id;
    return (
      <tr key={r.id}>
        <td>{r.section.name}</td>
        <td><a data-tag={r.id} href={url} onClick={f} >{r.firstName} {r.lastName}</a></td>
        <td>{r.theme}</td>
        <td>
          <button className="btn btn-danger" onClick={() => { if (window.confirm('Are you sure you wish to delete this item?')) this.props.onDelete(r.id) } } >
            X
          </button>
        </td>
      </tr>
    );
    }else{
    return (<tr key={r.id}><td>{r.section.name}</td><td>{r.firstName} {r.lastName}</td><td>{r.theme}</td><td></td></tr>);
    }

  }
  onSelect(e){
    e.preventDefault();
    const requestId = e.target.dataset.tag;
    this.props.onSelect(parseInt(requestId));
  }
  render(){

    const approved = this.props.requests.filter(r => r.approved === 1).map((request) => this.tablerow(request, this.onSelect));
    const unapproved = this.props.requests.filter(r => r.approved !== 1).map((request) => this.tablerow(request, this.onSelect));
    return (
      <div>
      <span>Одобренные</span>
      <table className="table table-condensed table-striped">
        <thead>
          <tr><th>Секция</th><th>ФИО</th><th>Тема</th><th></th></tr>
        </thead>
         <tbody>
          {approved}
         </tbody>
      </table>
      <span>На рассмотрении</span>
      <table className="table table-condensed table-striped">
        <thead>
          <tr><th>Секция</th><th>ФИО</th><th>Тема</th><th></th></tr>
         </thead>
         <tbody>
          {unapproved}
         </tbody>
      </table>
      </div>
    );
  }
}

class RequestsForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      id: props.request.id, 
      email:props.request.email, 
      section:props.request.section, 
      sectionId:props.request.section.id, 
      firstName:props.request.firstName, 
      lastName:props.request.lastName, 
      theme:props.request.theme, 
      approved:props.request.approved
    };
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onReturn = this.onReturn.bind(this);
  }
  onSubmitForm(){
   var t = this;
    var request = {
      id:this.state.id,
      sectionId:this.state.sectionId, 
      email:this.state.email, 
      firstName: this.state.firstName, 
      lastName: this.state.lastName, 
      theme: this.state.theme,
      approved: this.state.approved
    };
    Api.saveRequest(request, function(data){
        t.setState({
          id: data.id, 
          email:data.email, 
          section:data.section, 
          sectionId:parseInt(data.section.id),
          firstName:data.firstName, 
          lastName:data.lastName, 
          theme:data.theme, 
          approved:parseInt(data.approved)
        });
    });
  }
  onChangeInput(e){
    const name = e.target.name;
    const value = e.target.type === 'checkbox' ? (e.target.checked === true ? 1 : 0) : e.target.value;
    this.setState({[name]: value});
  }
  onReturn(e){
    e.preventDefault();
    this.props.onReturn({
          id: this.state.id, 
          email:this.state.email, 
          section:this.state.section, 
          firstName:this.state.firstName, 
          lastName:this.state.lastName, 
          theme:this.state.theme, 
          approved:this.state.approved
    });

  }
  render(){
    const id = this.state.id;
    const name = this.state.name;
    const email = this.state.email;
    const sectionId = this.state.sectionId;
    const firstName = this.state.firstName;
    const lastName = this.state.lastName;
    const theme = this.state.theme;
    const approved = this.state.approved;

    return (
      <div>
      <a href="" onClick={this.onReturn}>&lt; Вернуться к списку</a>
      <form className="form form-horizontal">
        <TextInput label="ИД" placeholder="ИД" name="id" value={id} onChange={this.onChangeInput} />
        <EmailInput label="Электронная почта" placeholder="post@example.com" name="email" value={email} onChange={this.onChangeInput} />
        <SectionComboBox name="sectionId" value={sectionId} onChange={this.onChangeInput}/>
        <TextInput label="Имя" placeholder="Имя" name="firstName" value={firstName} onChange={this.onChangeInput} />
        <TextInput label="Фамилия" placeholder="Фамилия" name="lastName" value={lastName} onChange={this.onChangeInput} />
        <TextInput label="Тема доклада" placeholder="Тема доклада" name="theme" value={theme} onChange={this.onChangeInput} />
        <CheckBox label="Подтверждена" placeholder="Подтверждена" name="approved" value={approved} onChange={this.onChangeInput} />
        <div className="form-group" />
        <Button label="Изменить заявку" onClick={this.onSubmitForm} />
      </form>
      </div>
    );
  }
}
function RequestsPage() {
  const [requests, setRequests] = React.useState([]);
  const [current, setCurrent] = React.useState(null);
  const [initialized, setInitialized] = React.useState(false);
  const profile = React.useContext(UserProfileContext).profile;

  React.useEffect(function(){
    if (!initialized){
      Api.loadRequests(function(data){
        for(var i in data){
          data[i].approved = parseInt(data[i].approved);
          data[i].section.id= parseInt(data[i].section.id);
          data[i].id= parseInt(data[i].id);
        }
        setRequests(data);
      });
      setInitialized(true);
    }
  });

  function onDelete(requestId){
    Api.deleteRequest(requestId, function(data){
      setInitialized(false);
    });
  }
  function onReturn(current){
    for(var i in requests){
      if(requests[i].id === current.id){
        requests[i] = current;
      }
    }
    setCurrent(null);
    setRequests(requests);
    
  }
  function onSelect(requestId){
    const c = requests.find(r => r.id === requestId);
    setCurrent(c);
  }

  var component;
  if (current === null){
    component = <RequestsList requests={requests} onSelect={onSelect} onDelete={onDelete} profile={profile}/>
  }else if(profile.isAuthenticated){
    component = <RequestsForm request={current} onReturn={onReturn} profile={profile}/>
  }else{
    component = <span/>
  }

  return (
    <>
    <div id="center" className="col-sm-12">
      {component}
    </div>
    </>
  );
}

export {RequestsPage};
