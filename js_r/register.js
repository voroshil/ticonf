import React from 'react';
import Api from 'api';
import {Button, EmailInput, TextInput, ComboBox, SectionComboBox} from 'app';

class RequestsInfo extends React.Component {
  render() {
    return (
      <div>
      <table className="table table-condensed">
        <thead>
        <tr><th>Всего подано заявок</th></tr>
        </thead>
        <tbody>
        <tr><td>{this.props.requests} </td></tr>
        </tbody>
      </table>
      <table className="table table-condensed">
        <thead>
          <tr><th>Из них одобрено</th></tr>
        </thead>
        <tbody>
          <tr><td>{this.props.approved}</td></tr>
        </tbody>
      </table>
     </div>
    );
  }
}

class RegisterForm extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      sectionId: "",
      email: "",
      firstName: "",
      lastName: "",
      theme: "",
      isSubmitDisabled: false
    }

    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
  }
  onSubmitForm(){
    if (this.state.isSubmitDisabled)
      return;
    this.setState({isSubmitDisabled: true});
    var request = {
      sectionId: this.state.sectionId, 
      email: this.state.email, 
      firstName: this.state.firstName, 
      lastName: this.state.lastName, 
      theme: this.state.theme
    };
    var t = this;
    Api.register(request, function(data){
        t.setState({
          sectionId: parseInt(data.section.id),
          email: data.email === null ? "" : data.email,
          firstName: data.firstName === null ? "" : data.firstName,
          lastName: data.lastName === null ? "" : data.lastName,
          theme: data.theme === null ? "" : data.theme,
          isSubmitDisabled: false
        });
    }, function(error){
      t.setState({isSubmitDisabled: false});
    });

    console.log(this.state);
  }
  onChangeInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value});
  }
  render(){
    const sections = this.state.sections;
    const sectionId = this.state.sectionId;
    const email = this.state.email;
    const firstName = this.state.firstName;
    const lastName = this.state.lastName;
    const theme = this.state.theme;
    return (
      <form className="form form-horizontal">
        <SectionComboBox name="sectionId" value={sectionId} onChange={this.onChangeInput}/>
        <EmailInput label="Электронная почта" placeholder="post@example.com" name="email" value={email} onChange={this.onChangeInput} />
        <TextInput label="Имя" placeholder="Имя" name="firstName" value={firstName} onChange={this.onChangeInput} />
        <TextInput label="Фамилия" placeholder="Фамилия" name="lastName" value={lastName} onChange={this.onChangeInput} />
        <TextInput label="Тема доклада" placeholder="Тема доклада" name="theme" value={theme} onChange={this.onChangeInput} />
        <Button label="Подать заявку" disabled={this.state.isSubmitDisabled} onClick={this.onSubmitForm} />
      </form>
    );
  }
}

class RegisterPage extends React.Component {
  constructor(props){
    super(props);
  }
  render(){
    return (
      <>
      <div id="center" className="col-sm-12">
        <RegisterForm />
      </div>
      </>
    );
  }
}

export {RegisterPage};

