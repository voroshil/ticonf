import React from 'react';
import Api from 'api';
import {TextInput} from 'app';

class SectionsList extends React.Component {
  constructor(props){
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }
  tablerow(s, f){
    if (this.props.profile.isAuthenticated){
    const url = "#!/sections/" + s.id;
    return (
      <tr key={s.id}>
        <td><a data-tag={s.id} href={url} onClick={f}>{s.name}</a></td>
        <td>{s.capacity}</td>
      </tr>
    );
    }else{
    return (
      <tr key={s.id}>
        <td>{s.name}</td>
        <td>{s.capacity}</td>
      </tr>
    );
    }
  }
  onSelect(e){
    e.preventDefault();
    const sectionId = e.target.dataset.tag;
    this.props.onSelect(parseInt(sectionId));
  }
  render(){
    var addComponent;
    if (this.props.profile.isAuthenticated){
      addComponent = <a href="" onClick={this.onAddNew}>Добавить</a>
    }else{
      addComponent = <span/>
    }
    const sections = this.props.sections.map((section) => this.tablerow(section, this.onSelect));
    return (
      <div>
        {addComponent}
        <table className="table table-condensed table-striped">
          <thead>
            <tr><td>Название</td><td>Вместимость</td></tr>
          </thead>
          <tbody>
            {sections}
          </tbody>
        </table>
      </div>
    );
  }
}

class SectionsForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      id: props.section.id, 
      name: props.section.name,
      capacity: props.section.capacity,
      type: props.section.type
    };
    this.onChangeInput = this.onChangeInput.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onReturn = this.onReturn.bind(this);
  }
  onSubmitForm(){
    var t= this;
    Api.saveSection(
      {
        id:t.state.id, 
        name:t.state.name, 
        capacity:t.state.capacity, 
        type:t.state.type
      },
      function(data){ 
        t.setState({id:data.id, name:data.name, capacity: data.capacity});
      }
    );
  }
  onChangeInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]: value});
  }
  onReturn(e){
    e.preventDefault();
    this.props.onReturn({
      id: this.state.id, 
      name: this.state.name,
      capacity: this.state.capacity,
      type: this.state.type
    });
  }
  render(){
    const id = this.state.id;
    const name = this.state.name;
    const capacity = this.state.capacity;
    const type = this.state.type;

    return (
      <div>
      <a href="" onClick={this.onReturn}>&lt; Вернуться к списку</a>
      <form className="form form-horizontal">
      <TextInput label="ИД" placeholder="ИД" name="id" value={id} onChange={this.onChangeInput} />
      <TextInput label="Название" placeholder="Название" name="name" value={name} onChange={this.onChangeInput} />
      <TextInput label="Вместимость" placeholder="Вместимость" name="capacity" value={capacity} onChange={this.onChangeInput} />
      <TextInput label="Тип" placeholder="Тип" name="type" value={type} onChange={this.onChangeInput} />
      <Button label="Изменить секцию" onClick={this.onSubmitForm} />
      </form>
      </div>
    );
  }
}

function SectionsPage() {
  const [sections, setSections] = React.useState([]);
  const [current, setCurrent] = React.useState(null);
  const [initialized, setInitialized] = React.useState(false);
  const profile = React.useContext(UserProfileContext).profile;

  React.useEffect(function(){
    if(!initialized){
      Api.loadSections(function(data){
        for(var i in data){
          data[i].id= parseInt(data[i].id);
        }
        setSections(data);
      });
      setInitialized(true);
    }
  });

  function onReturn(current){
    for (var i in sections){
      if (sections[i].id === current.id){
        sections[i] = current;
      }
    }
    setCurrent(null);
    setSections(sections);
  }
  function onSelect(sectionId){
    const c = sections.find(s => s.id === sectionId);
    setCurrent(c);
  }

  var component;
  if (current === null){
    component = <SectionsList sections={sections} onSelect={onSelect} profile={profile}/>
  }else if (profile.isAuthenticated){
    component = <SectionsForm section={current} onReturn={onReturn} profile={profile} />
  }else{
    component = <span/>
  }
  return (
     <>
      <div id="center" className="col-sm-12">
        {component}
      </div>
     </>
  );
}

export {SectionsPage};
