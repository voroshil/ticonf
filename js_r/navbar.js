import React from 'react';
import {Link} from 'react-router-dom';

function NavPublicLink({to, label}){
  return (<li key={to}><Link to={to}>{label}</Link></li>);
}
function NavPrivateExternalLink({to, label}){
  const profile = React.useContext(UserProfileContext).profile;
  if (profile.isAuthenticated){
    return (<li key={to}><a href={to} target="_blank">{label}</a></li>);
  }else{
    return (<span/>);
  }
}
function NavPrivateLink({to, label}){
  const profile = React.useContext(UserProfileContext).profile;
  if (profile.isAuthenticated){
    return (<li key={to}><Link to={to}>{label}</Link></li>);
  }else{
    return (<span/>);
  }
}

function NavSign(){
  const profile = React.useContext(UserProfileContext).profile;
  if (profile.isAuthenticated){
    return (
      <NavPublicLink to="/logout" label="Выход" />
    );
  }else{
    return (
      <NavPublicLink to="/login" label="Вход" />
    );
  }
}
class Navbar extends React.Component {
  render(){
    return (
      <div id="horizontalnav" className="row">
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <Link to="#" className="navbar-brand">
                      <img src="images/logo.jpg" width="32" height="32"/>
                    </Link>
                </div>

                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav">
                      <NavPublicLink to="/" label="Главная" />
		      <NavPublicLink to="/sections" label="Секции" />
                      <NavPublicLink to="/requests" label="Участники" />
                      <NavPublicLink to="/register" label="Регистрация"/>
                      <NavPrivateLink to="/settings" label="Настройки" />
                      <NavPrivateExternalLink to="api/export.php" label="Экспорт" />
                    </ul>

                    <ul className="nav navbar-nav navbar-right">
                      <NavSign />
                    </ul>
                </div>
            </div>
        </nav>
      </div>

    );
  }
}

export {Navbar};
