import React from 'react';
import Api from 'api';
import {HashRouter, Route, Redirect} from 'react-router-dom';
import {LoginPage, LogoutPage, Navbar, SettingsPage, RequestsPage, RegisterPage, SectionsPage} from 'app';

class DefaultPage extends React.Component {
  render(){
    return (
      <>
      <div id="leftgutter" className="col-sm-4">
        <RequestsInfo requests={this.props.total} approved={this.props.approved} />
      </div>
      <div id="center" className="col-sm-8">
        {this.props.welcome}
      </div>
      </>
    );
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    Api.isAuthenticated() 
      ? <Component {...props} />
      : <Redirect to={{pathName:'/login', state: {from: props.location} }} />
  )} />
)

const UserProfileContext = React.createContext();
class App extends React.Component{
  constructor(props){
    super(props);

    console.log(Api);
    this.state = {
      header: 'Добро пожаловать на сайт конференции',
      footer: 'Здесь будет контактная информация',
      welcome: 'Добро пожаловать на конференцию',
      profile: {isAuthenticated: Api.isAuthenticated()},
      approved: 0,
      total: 0
    };
    this.setProfileState = this.setProfileState.bind(this);
  }
  componentDidMount(){
    var t = this;
    Api.loadWelcome(function(data){
        t.setState({header: data.header, footer: data.footer, welcome: data.welcome, total: data.total, approved: data.approved});
    });
  }
  setProfileState(profile){
    this.setState({profile: profile});
  }
  render(){
    const value = {profile: this.state.profile, setProfile: this.setProfileState};
    return (
      <HashRouter>
      <UserProfileContext.Provider value={value}>
        <div id="topHeader" className="row">
          {this.state.header}
        </div>
        <Navbar />
        <div className="row">
        <Route exact path='/' render={(props) => <DefaultPage {...props} welcome={this.state.welcome} approved={this.state.approved} total={this.state.total} />} />
        <Route exact path='/login' component={LoginPage} />
        <Route exact path='/logout' component={LogoutPage} />
        <Route exact path='/sections' component={SectionsPage} />
        <Route exact path='/requests' component={RequestsPage} />
        <Route exact path='/register' component={RegisterPage} />
        <PrivateRoute exact path='/settings' component={SettingsPage} />
        </div>
        <div id="footer" className="row">
          {this.state.footer}
        </div>
      </UserProfileContext.Provider>
      </HashRouter>
    );
  }
}

export {App};

