import React from 'react';

class CheckBox extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e){
    this.props.onChange(e);
  }
  render(){
    const value = this.props.value;
    const label = this.props.label;
    const placeholder = this.props.placeholder;
    const name = this.props.name;
    
    return (
        <div className="checkbox">
          <label>
            <input type="checkbox" id={name} name={name} className="form-check-input" placeholder={placeholder} value={value} onChange={this.onChange}/> {label}
           </label>
        </div>
    );
  }
}
class Button extends React.Component {
  constructor(props){
    super(props);
    this.onClick = this.onClick.bind(this);
  }
  onClick(e){
    this.props.onClick();
  }
  render(){
    const label = this.props.label;
    const disabled = this.props.disabled;
    return (
      <div className="form-group">
        <button type="button" className="btn btn-primary" disabled={disabled} onClick={this.onClick}>{label}</button>
      </div>
    );
  }
}
class ComboBox extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e){
    this.props.onChange(e);
  }
  render(){
    const value = this.props.value;
    const options = this.props.options.map((option) => <option value={option.id} key={option.id}>{option.name}</option>);
    const label = this.props.label;
    const placeholder = this.props.placeholder;
    const name = this.props.name;

    return (
        <div className="form-group">
          <label htmlFor={name}>{label}</label>
          <select id={name} name={name} className="form-control" placeholder={placeholder} value={value} onChange={this.props.onChange}>
            {options}
          </select>
        </div>
    );
  }
}

class SectionComboBox extends React.Component {
  constructor(props){
    super(props);
    this.state = {sections:[]};
  }

  componentDidMount(){
    var t = this;
    Api.loadSections(function(data){
      t.setState({sections: data});
    });
  }
  render(){
    const sections = this.state.sections;
    const sectionId = this.props.value;
    const name = this.props.name;
    return (
      <ComboBox label="Секция" placeholder="Выберите секцию" name={name} value={sectionId} options={sections} onChange={this.props.onChange}/>
    );
  }
}

class EmailInput extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e){
    this.props.onChange(e);
  }
  render(){
    const value = this.props.value;
    const label = this.props.label;
    const placeholder = this.props.placeholder;
    const name = this.props.name;

    return (
        <div className="form-group">
          <label htmlFor={name}>{label}</label>
          <input type="email" id={name} name={name} className="form-control" placeholder={placeholder} value={value} onChange={this.onChange}/>
        </div>
    );
  }
}

class TextInput extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e){
    this.props.onChange(e);
  }
  render(){
    const value = this.props.value;
    const label = this.props.label;
    const placeholder = this.props.placeholder;
    const name = this.props.name;

    return (
        <div className="form-group">
          <label htmlFor={name}>{label}</label>
          <input type="text" id={name} name={name} className="form-control" placeholder={placeholder} value={value} onChange={this.onChange}/>
        </div>
    );
  }
}
class PasswordInput extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e){
    this.props.onChange(e);
  }
  render(){
    const value = this.props.value;
    const label = this.props.label;
    const placeholder = this.props.placeholder;
    const name = this.props.name;

    return (
        <div className="form-group">
          <label htmlFor={name}>{label}</label>
          <input type="password" id={name} name={name} className="form-control" placeholder={placeholder} value={value} onChange={this.onChange}/>
        </div>
    );
  }
}

class TextArea extends React.Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e){
    this.props.onChange(e);
  }
  render(){
    const value = this.props.value;
    const label = this.props.label;
    const placeholder = this.props.placeholder;
    const name = this.props.name;

    return (
        <div className="form-group">
          <label htmlFor={name}>{label}</label>
          <textarea id={name} name={name} className="form-control" placeholder={placeholder} value={value} onChange={this.onChange}/>
        </div>
    );
  }
}

export {Button, ComboBox, CheckBox, EmailInput, PasswordInput, TextInput, TextArea, SectionComboBox};

