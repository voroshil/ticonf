var Api = new ApiClass();

function ApiClass(){
  this.profile = {
    isAuthenticated: false,
    token: null,
    email: null,
    name: null
  };
}

ApiClass.prototype.isAuthenticated = function(){
  return this.profile.isAuthenticated;
}
ApiClass.prototype.login = function(username, password, cbSuccess, cbError){
  var request = {username: username, password: password};
  var t = this;
  $.ajax({
    url: 'api/login.php',
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: "json",
    data: JSON.stringify(request),
      success: function(data){
        t.profile.isAuthenticated = true;
        t.profile.token = data.conftoken;
        if (typeof(cbSuccess) == 'function'){
          cbSuccess();
        }
      },
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
  });
}

ApiClass.prototype.logout = function(){
  var t = this;
  $.ajax({
    url: 'api/logout.php',
    type: 'POST',
    contentType: 'application/json; charset=utf-8',
    dataType: "json",
    data: "",
    error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
  });
  this.profile.isAuthenticated = false; 
  this.profile.token = null;
  this.profile.email = null;
  this.profile.name = null;
}
ApiClass.prototype.register = function(request, cbSuccess, cbError){
  var t = this;
    $.ajax({
      url: 'api/register.php',
      type: 'POST',
      contentType: 'application/json; charset=utf-8',
      dataType: "json",
      data: JSON.stringify(request),
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}
ApiClass.prototype.loadWelcome = function(cbSuccess, cbError){
  var t = this;
  $.ajax({
    url: 'api/welcome.php',
    headers: {
      'ApiClass-Key': t.profile.token
    },
    type: 'GET',
    success: cbSuccess,
    error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
  });
}
ApiClass.prototype.loadSettings = function(cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/settings.php',
      headers: {
        'ApiClass-Key': t.profile.token
      },
      type: 'GET',
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}
ApiClass.prototype.saveSettings = function(settings, cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/settings.php',
      headers: {
        'ApiClass-Key': t.profile.token
      },
      type: 'POST',
      dataType: "json",
      data: JSON.stringify(settings),
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}
ApiClass.prototype.loadSections = function(cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/sections.php',
      headers: {
        'ApiClass-Key': t.profile.token
      },
      type: 'GET',
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}
ApiClass.prototype.saveSection = function(request, cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/sections.php',
      headers: {
        'ApiClass-Key': t.profile.token
      },
      type: 'POST',
      contentType: 'application/json; charset=utf-8',
      dataType: "json",
      data: JSON.stringify(request),
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}

ApiClass.prototype.loadRequests = function(cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/requests.php',
      type: 'GET',
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
  }
ApiClass.prototype.saveRequest = function(request, cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/requests.php',
      headers: {
        'ApiClass-Key': t.profile.token
      },
      type: 'POST',
      contentType: 'application/json; charset=utf-8',
      dataType: "json",
      data: JSON.stringify(request),
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}
ApiClass.prototype.deleteRequest = function(requestId, cbSuccess, cbError){
    var t = this;
    $.ajax({
      url: 'api/requests.php',
      headers: {
        'ApiClass-Key': t.profile.token
      },
      type: 'DELETE',
      contentType: 'application/json; charset=utf-8',
      dataType: "json",
      data: JSON.stringify({id: requestId}),
      success: cbSuccess,
      error: typeof(cbError)==='function' ? cbError :  t.defaultFailCallback
    });
}

ApiClass.prototype.defaultFailCallback = function(data){
  console.log("Error API call:");
  console.log(data);
}
