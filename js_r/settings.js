import React from 'react';
import Api from 'api';
import {Button, TextArea} from 'app';

function SettingsForm() {
  const [settings, setSettings] = React.useState({});
  const [initialized, setInitialized] = React.useState(false);

  React.useEffect(function(){
    if(!initialized){
      Api.loadSettings(function(data){
        setSettings(data);
      });
      setInitialized(true);
    }
  });

  function tablerow(name, s){
    return (
      <TextArea key={name} name={name} label={name} value={s[name]} onChange={onChangeInput}/>
    );
  }
  function onSubmitForm(){
    Api.saveSettings(settings, function(data){
      setSettings(data);
    });
  }
  function onChangeInput(e){
    const name = e.target.name;
    const value = e.target.value;
    setSettings({...settings, [name]: value});
  }
  var s = [];
  for(var i in settings){
    s.push(tablerow(i, settings));
  }
  if (s.length > 0){
    return (
      <form className="form form-horizontal" >
      {s}
      <Button label="Сохранить" onClick={onSubmitForm}/>
      </form>
    );
  }else{
    return (<span/>);
  }
}


function SettingsPage() {
  return (
      <>
      <div id="center" className="col-sm-12">
        <SettingsForm />
      </div>
      </>
    );
}

export {SettingsPage};
