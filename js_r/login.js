import React from 'react';
import Api from 'api';
import {Redirect} from 'react-router-dom';

function LogoutPage() {
  const {profile, setProfile} = React.useContext(UserProfileContext);

  Api.logout();
  profile.isAuthenticated = false;
  setProfile(profile);
  
  return (
    <Redirect to="/" />
  );
}

function LoginPage() {
  const {profile, setProfile} = React.useContext(UserProfileContext);

  function onLogin(username, password){
    Api.login(username, password, function(){
      profile.isAuthenticated = true;
      setProfile(profile);
    });
  }
  if (Api.isAuthenticated()) {
    return (<Redirect to="/" />);
  }
  return (
      <div className="col-sm-10 col-sm-push-1">
        <LoginForm onLogin={onLogin} />
      </div>
  );
}

class LoginForm extends React.Component{
  constructor(props){
    super(props);
    this.state = { username: "", password: "" }
    this.onLogin = this.onLogin.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
  }
  onChangeInput(e){
    const name = e.target.name;
    const value = e.target.value;
    this.setState({[name]:value});
  }
  onLogin(){
    this.props.onLogin(this.state.username, this.state.password);
  }
  render(){
    return (
      <form className="form form-horizontal">
        <TextInput label="Логин" name="username" onChange={this.onChangeInput} />
        <PasswordInput label="Пароль" name="password" onChange={this.onChangeInput} />
        <Button label="Войти" onClick={this.onLogin} />
      </form>
    );
  }
}

export {LoginPage, LogoutPage};
