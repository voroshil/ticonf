<?php 

require_once("include/auth.inc");
require_once("include/settings.inc");
require_once("include/model/request.inc");
require_once("include/model/section.inc");

function send_mail($r){
    $message = settings_get(CFG_NAME_MESSAGE);
    $smtp_host = settings_get(CFG_NAME_SMTP_HOST);
    $smtp_port = settings_get(CFG_NAME_SMTP_PORT);

    $message = str_replace("[[firstName]]", $r->firstName, $message);
    $message = str_replace("[[lastName]]", $r->lastName, $message);
    $message = str_replace("[[email]]", $r->email, $message);
    $message = str_replace("[[theme]]", $r->theme, $message);
    $message = str_replace("[[section]]", $r->section->name, $message);
    $mail = array(
      'to' => mb_encode_mimeheader($r->firstName." ".$r->lastName)." <".$r->email.">",
      'subject' => mb_encode_mimeheader(settings_get(CFG_NAME_SUBJECT)),
      'message' => $message,
      'headers' => "MIME-Version: 1.0"."\r\n"."Content-type: text/plain; charset=utf-8"."\r\n"."From: ".settings_get(CFG_NAME_FROM)."\r\n",
      'host' => $smtp_host,
      'port' => $smtp_port
    );
    ini_set('SMTP', $smtp_host);
    ini_set('smtp_port', $smtp_port);
    date_default_timezone_set('Etc/GMT+6');
    mail($mail['to'],$mail['subject'],$mail['message'], $mail['headers']);
    $r->message = json_encode($mail);
}

function validate_request($obj){
  if (!isset($obj->sectionId, $obj->email, $obj->firstName, $obj->lastName, $obj->theme)){
    return FALSE;
  }
  if ($obj->email == '' || $obj->firstName == '' || $obj->lastName == ''){
    return FALSE;
  }
  if (!filter_var($obj->email, FILTER_VALIDATE_EMAIL)){
    return FALSE;
  }
  return TRUE;
}
function process_post(){
  $post_data = file_get_contents("php://input");
  $obj = json_decode($post_data);

  if (!validate_request($obj)){
    http_response_code (400);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $section = Section::loadFromDbById($obj->sectionId);
  $need_mail = FALSE;
  if (isset($obj->id)){
    $r = Request::loadFromDbById($obj->id);
    $r->section = $section;
    $r->email = $obj->email;
    $r->firstName = $obj->firstName;
    $r->lastName = $obj->lastName;
    $r->theme = $obj->theme;
  }else{
    if (!Request::checkCapacity($obj->sectionId)){
      http_response_code (406);
      header("Content-type: application/json; charset=UTF-8");
      $r = array('message' => 'Limit reached');
      echo json_encode($r);
      return;
    }
    $r = new Request(null, $section, $obj->email, $obj->firstName, $obj->lastName, $obj->theme, 0);
    $need_mail = TRUE;
  }
  if(isset($obj->approved)){
    if ($r->approved == 0 && $obj->approved == 1){
      $need_mail = TRUE;
    }
    $r->approved = $obj->approved;
  }
  $r->saveToDb();
  $r->loadFromDb();
  //Debug
  $need_mail = TRUE;
  if ($need_mail){
    send_mail($r);
  }
  echo json_encode($r);
}

switch($_SERVER['REQUEST_METHOD']){
  case 'POST': process_post(); break;
}

?>
