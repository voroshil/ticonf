<?php

require_once("include/auth.inc");

function process_post(){
  auth_logout();

  header("Content-type: application/json; charset=UTF-8");
  echo "{}";
}

switch($_SERVER['REQUEST_METHOD']){
//  case 'GET': process_get(); break;
  case 'POST': process_post(); break;
}

?>
