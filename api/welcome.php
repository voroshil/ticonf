<?php

require_once("include/auth.inc");
require_once('include/settings.inc');
require_once("include/model/request.inc");

function process_get(){
  $totals = Request::totals();
  $settings = settings_get_all();

  $result = array(
    'header' => $settings[CFG_NAME_HEADER],
    'footer' => $settings[CFG_NAME_FOOTER],
    'welcome' => $settings[CFG_NAME_WELCOME],
    'approved' => $totals['approved'],
    'unapproved' => $totals['unapproved'],
    'total' => $totals['total']
  );

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($result);
}

switch($_SERVER['REQUEST_METHOD']){
  case 'GET': process_get(); break;
}

?>
