<?php

require_once("include/auth.inc");
require_once('include/settings.inc');

function process_get(){

  $settings = settings_get_all();

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($settings);
}

function process_post(){
  $post_data = file_get_contents("php://input");
  $obj = json_decode($post_data);
  settings_set_all($obj);
  
  $settings = settings_get_all();

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($settings);
}

if (!auth_check()){
    http_response_code (403);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
}

switch($_SERVER['REQUEST_METHOD']){
  case 'GET': process_get(); break;
  case 'POST': process_post(); break;
}

?>
