<?php 

require_once("include/model/request.inc");
require_once("include/auth.inc");

function process_get(){
  $requests = Request::loadAllFromDb();

  header("Content-type: text/plain; charset=UTF-8");

  echo "\"ID\";\"Approved\";\"section_id\";\"section_name\";\"email\";\"first_name\";\"last_name\";\"theme\"\n";
  foreach($requests as $r){
    $id = $r->id;
    $approved = $r->approved;
    $section_id = $r->section->id;
    $section_name = $r->section->name;
    $email = $r->email;
    $firstName = $r->firstName;
    $lastName = $r->lastName;
    $theme = $r->theme;
    echo "$id;$approved;$section_id;\"$section_name\";\"$email\";\"$firstName\";\"$lastName\";\"$theme\"\n";
  }
}


if (!auth_check()){
    http_response_code (403);
    return;
}

switch($_SERVER['REQUEST_METHOD']){
  case 'GET': process_get(); break;
}

?>
