<?php

require_once("include/auth.inc");

function process_post(){
  $post_data = file_get_contents("php://input");
  $obj = json_decode($post_data);

  if (!isset($obj->username, $obj->password)){
    http_response_code (400);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  if (!auth_login($obj->username, $obj->password)){
    http_response_code (403);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $result = array('conftoken' => $_SESSION['token']);

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($result);
}

switch($_SERVER['REQUEST_METHOD']){
//  case 'GET': process_get(); break;
  case 'POST': process_post(); break;
}

?>
