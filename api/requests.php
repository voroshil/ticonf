<?php 

require_once("include/auth.inc");
require_once("include/settings.inc");
require_once("include/model/request.inc");
require_once("include/model/section.inc");

function process_get(){

  $requests = Request::loadAllFromDb();

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($requests);
}

function process_delete(){
  if (!auth_check()){
    http_response_code (403);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $post_data = file_get_contents("php://input");
  $obj = json_decode($post_data);

  if (!isset($obj->id)){
    http_response_code (400);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $r = Request::loadFromDbById($obj->id);
  $r->deleteFromDb();

  echo "{}";
  return;
}
function process_post(){
  if (!auth_check()){
    http_response_code (403);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $post_data = file_get_contents("php://input");
  $obj = json_decode($post_data);

  if (!isset($obj->sectionId, $obj->email, $obj->firstName, $obj->lastName, $obj->theme)){
    http_response_code (400);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $section = Section::loadFromDbById($obj->sectionId);
  $need_mail = FALSE;
  if (isset($obj->id)){
    $r = Request::loadFromDbById($obj->id);
    $r->section = $section;
    $r->email = $obj->email;
    $r->firstName = $obj->firstName;
    $r->lastName = $obj->lastName;
    $r->theme = $obj->theme;
  }else{
    if (!Request::checkCapacity($obj->sectionId)){
      return;
    }
    $r = new Request(null, $section, $obj->email, $obj->firstName, $obj->lastName, $obj->theme, 0);
    $need_mail = TRUE;
  }
  if(isset($obj->approved)){
    if ($r->approved == 0 && $obj->approved == 1){
      $need_mail = TRUE;
    }
    $r->approved = $obj->approved;
  }
  $r->saveToDb();
  $r->loadFromDb();

  echo json_encode($r);
}

switch($_SERVER['REQUEST_METHOD']){
  case 'GET': process_get(); break;
  case 'POST': process_post(); break;
  case 'DELETE': process_delete(); break;
}

?>
