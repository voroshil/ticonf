<?php 

require_once("include/auth.inc");
require_once('include/model/section.inc');

function process_get(){

  $sections = Section::loadAllFromDb();

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($sections);
}

function process_post(){
  if (!auth_check()){
    http_response_code (403);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  $post_data = file_get_contents("php://input");
  $obj = json_decode($post_data);
  if (!isset($obj->name, $obj->capacity, $obj->type)){
    http_response_code (400);
    header("Content-type: application/json; charset=UTF-8");
    echo "{}";
    return;
  }

  if (isset($obj->id)){
    $section = Section::loadFromDbById($obj->id);
    $section->name = $obj->name;
    $section->capacity = $obj->capacity;
    $section->type = $obj->type;
  }else{
    $section = new Section(null, $obj->name, $obj->capacity, $obj->type);
  }
  $section->saveToDb();
  // Id will be updated during insert
  $section->loadFromDb();

  header("Content-type: application/json; charset=UTF-8");
  echo json_encode($section);
}
switch($_SERVER['REQUEST_METHOD']){
  case 'GET': process_get(); break;
  case 'POST': process_post(); break;
}

?>
