<?php 

require_once("include/db.inc");

define('CFG_NAME_FROM', 'MAIL_FROM');
define('CFG_NAME_SUBJECT', 'MAIL_SUBJECT');
define('CFG_NAME_MESSAGE', 'MAIL_MESSAGE');
define('CFG_NAME_HEADER', 'SITE_HEADER');
define('CFG_NAME_FOOTER', 'SITE_FOOTER');
define('CFG_NAME_WELCOME', 'SITE_WELCOME');
define('CFG_NAME_SMTP_HOST', 'SMTP_HOST');
define('CFG_NAME_SMTP_PORT', 'SMTP_PORT');


function settings_get_defaults(){
  return array(
  CFG_NAME_SMTP_HOST => 'localhost',
  CFG_NAME_SMTP_PORT => '25',
  CFG_NAME_FROM => 'post@example.com',
  CFG_NAME_SUBJECT => 'Registration confirmed',
  CFG_NAME_MESSAGE => 'Dear, [[firstName]] [[lastName]]!'."\r\n".
'You are successfully registered to our conference,'."\r\n".
'section "[[section]]"'."\r\n\r\n".
'Will be glad to see you!'."\r\n\r\n".
'With aplogies,'."\r\n".
'Administration team.'."\r\n",
  CFG_NAME_HEADER => 'Добро пожаловать на сайт конференции',
  CFG_NAME_FOOTER => 'Здесь будет контактная информация',
  CFG_NAME_WELCOME => 'Добро пожаловать на конференцию'
  );
}

function settings_get_all(){
  $file_db = db_connect();
  $stmt = $file_db->prepare('SELECT key as name, value from settings');
  $stmt->execute();
  $rowset = $stmt->fetchAll();
  $result = array();
  $cfg_defaults = settings_get_defaults();
  foreach($rowset as $row){
    if (isset($cfg_defaults[$row['name']])){
      $result[$row['name']] = $row['value'];
    }
  }
  foreach($cfg_defaults as $name => $value){
    if (!isset($result[$name])){
       $result[$name] = $value;
    }
  }
  return $result;
}
function settings_set_all($settings){
  if (!isset($settings))
    return;
  foreach($settings as $name => $value){
    settings_set($name, $value);
  }
}
function settings_get($name){
  $cfg_defaults = settings_get_defaults();
  if (!isset($name, $cfg_defaults[$name]))
    return null;
  $file_db = db_connect();
  $stmt = $file_db->prepare('SELECT value from settings where key=:key');
  $stmt->bindValue(':key', $name, SQLITE3_TEXT);
  $stmt->execute();
  $row = $stmt->fetch();
  if (isset($row['value'])){
    return $row['value'];
  }else{
    return $cfg_defaults[$name];
  }
}

function settings_set($name, $value){
  $cfg_defaults = settings_get_defaults();
  if (!isset($name, $value, $cfg_defaults[$name]))
    return null;
  $file_db = db_connect();
  $stmt = $file_db->prepare('SELECT value from settings where key=:key');
  $stmt->bindValue(':key', $name, SQLITE3_TEXT);
  $stmt->execute();
  $row = $stmt->fetch();
  if (isset($row['value'])){
    if ($row['value'] != $value){
      $stmt = $file_db->prepare('UPDATE settings SET value=:value where key=:key');
      $stmt->bindValue(':key', $name, SQLITE3_TEXT);
      $stmt->bindValue(':value', $value, SQLITE3_TEXT);
      $stmt->execute();
    }
  }else{
      $stmt = $file_db->prepare('INSERT INTO settings(key, value) VALUES(:key, :value)');
      $stmt->bindValue(':key', $name, SQLITE3_TEXT);
      $stmt->bindValue(':value', $value, SQLITE3_TEXT);
      $stmt->execute();
  }
}
?>
