<?php 

require_once("include/db.inc");
require_once("include/model/model.inc");

class Section extends ModelObject {
  public $name;
  public $capacity;
  public $type;

  function __construct($id, $name = null, $capacity = null, $type = null){
    parent::__construct($id);
    $this->name = $name;
    $this->capacity = $capacity;
    $this->type = $type;
  }

  public function loadFromDb(){
    $file_db = db_connect();
    $stmt = $file_db->prepare('SELECT name,capacity,section_type FROM sections where id=:id');
    $stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
    $stmt->execute();
    $row = $stmt->fetch();
    $this->name = $row['name'];
    $this->capacity = $row['capacity'];
    $this->type = $row['section_type'];
  }

  public static function loadFromDbById($id){
    if (!isset($id)){
      return;
    }
    $s = new Section($id);
    $s->loadFromDb();
    return $s;
  }

  public static function loadAllFromDb(){
    $file_db = db_connect();
    $stmt = $file_db->prepare('SELECT id, name,capacity,section_type FROM sections');
    $stmt->execute();
    $rowset = $stmt->fetchAll();
    $sections = array();
    foreach($rowset as $row){
      $s = new Section($row['id']);
      $s->name = $row['name'];
      $s->capacity = $row['capacity']; 
      $s->type=  $row['section_type'];
      array_push($sections, $s);
    }
    return $sections;
  }

  public function saveToDb(){
    $file_db = db_connect();
    if (!isset($this->id)) {
      $insert = "INSERT INTO sections (name, capacity, section_type) 
                VALUES (:name, :capacity, :type)";
      $stmt = $file_db->prepare($insert);
      $stmt->bindValue(':name', $this->name, SQLITE3_TEXT);
      $stmt->bindValue(':capacity', $this->capacity, SQLITE3_INTEGER);
      $stmt->bindValue(':type', $this->type, SQLITE3_INTEGER);
      $stmt->execute();    
      $stmt = $file_db->prepare('SELECT last_insert_rowid()');
      $stmt->execute();
      $row = $stmt->fetch();
      $this->id = $row[0];
    }else{
      $update = "UPDATE sections SET name=:name,capacity=:capacity,section_type=:type WHERE id=:id";
      $stmt = $file_db->prepare($update);
      $stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
      $stmt->bindValue(':name', $this->name, SQLITE3_TEXT);
      $stmt->bindValue(':capacity', $this->capacity, SQLITE3_INTEGER);
      $stmt->bindValue(':type', $this->type, SQLITE3_INTEGER);
      $stmt->execute();    
    }
  }
  public function getDeleteStatement(){
    return "DELETE FROM sections WHERE id=:id";
  }
}

?>
