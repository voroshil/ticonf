<?php 

require_once("include/db.inc");
require_once("include/model/model.inc");
require_once("include/model/section.inc");

class Request extends ModelObject {

  public $id;
  public $section;

  function __construct($id, $section = null, $email = null, $firstName = null, $lastName = null, $theme = null, $approved = null){
    parent::__construct($id);
    $this->section = $section;
    $this->email = $email;
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->theme = $theme;
    $this->approved = $approved;
  }
  public static function totals(){
    $result = array( 'approved' => 0, 'unapproved' => 0, 'total' => 0);
    $file_db = db_connect();
    $stmt = $file_db->prepare('SELECT approved, count(*) cnt FROM requests r GROUP BY approved');
    $stmt->execute();
    $rowset = $stmt->fetchAll();
    foreach($rowset as $row){
      if ((int)$row[0] === 1){
        $result['approved'] = (int)$row[1];
      }else if ((int)$row[0] === 0){
        $result['unapproved'] = (int)$row[1];
      }
    }
    $result['total'] = $result['approved'] + $result['unapproved'];

    return $result;
  }
  public static function checkCapacity($section_id){
    if (!isset($section_id)){
      return FALSE;
    }
    $file_db = db_connect();
    $section = Section::loadFromDbById($section_id);
    if ($section->capacity > 0){
      $stmt = $file_db->prepare('SELECT count(*) FROM requests r WHERE r.section_id=:section_id');
      $stmt->bindValue(':section_id', $section_id, SQLITE3_INTEGER);
      $stmt->execute();
      $row = $stmt->fetch();
      return (int)$row[0] < (int)$section->capacity;
    }else{
      return TRUE;
    }
  }
  public function loadFromDb(){
    $file_db = db_connect();
    $stmt = $file_db->prepare('SELECT r.*,s.name as section_name, s.capacity as section_capacity,s.section_type as section_type FROM requests r inner join sections s on r.section_id=s.id WHERE r.id=:id');
    $stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
    $stmt->execute();
    $row = $stmt->fetch();

    $this->section = new Section($row['section_id'], $row['section_name'], $row['section_capacity'], $row['section_type']);
    $this->email = $row['email'];
    $this->firstName = $row['first_name'];
    $this->lastName = $row['last_name'];
    $this->theme = $row['theme'];
    $this->approved = $row['approved'];
  }
  public static function loadFromDbById($id){
    if (!isset($id)){
      return;
    }
    $r = new Request($id);
    $r->loadFromDb();
    return $r;
  }
  public static function loadAllFromDb(){
    $file_db = db_connect();
    $stmt = $file_db->prepare('SELECT r.*,s.name as section_name, s.capacity as section_capacity,s.section_type as section_type FROM requests r inner join sections s on r.section_id=s.id');
    $stmt->execute();
    $rowset = $stmt->fetchAll();
    $requests = array();
    foreach($rowset as $row){
      $section = new Section($row['section_id'], $row['section_name'], $row['section_capacity'], $row['section_name']);
      $r = new Request($row['id'], $section, $row['email'], $row['first_name'], $row['last_name'], $row['theme'], $row['approved']);
      array_push($requests, $r);
    }
    return $requests;
  }
  public function saveToDb(){
    $file_db = db_connect();
    if (!isset($this->id)) {
      $insert = "INSERT INTO requests (section_id, email, first_name, last_name, theme, approved) 
                VALUES (:section_id, :email, :first_name, :last_name, :theme, :approved)";
      $stmt = $file_db->prepare($insert);
      $stmt->bindValue(':section_id', $this->section->id, SQLITE3_INTEGER);
      $stmt->bindValue(':email', $this->email, SQLITE3_TEXT);
      $stmt->bindValue(':first_name', $this->firstName, SQLITE3_TEXT);
      $stmt->bindValue(':last_name', $this->lastName, SQLITE3_TEXT);
      $stmt->bindValue(':theme', $this->theme, SQLITE3_TEXT);
      $stmt->bindValue(':approved', $this->approved, SQLITE3_INTEGER);
      $stmt->execute();    
      $stmt = $file_db->prepare('SELECT last_insert_rowid()');
      $stmt->execute();
      $row = $stmt->fetch();
      $this->id = $row[0];
    }else{
      $update = "UPDATE requests SET section_id=:section_id,email=:email,first_name=:first_name, last_name=:last_name, theme=:theme, approved=:approved WHERE id=:id";
      $stmt = $file_db->prepare($update);
      $stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
      $stmt->bindValue(':section_id', $this->section->id, SQLITE3_INTEGER);
      $stmt->bindValue(':email', $this->email, SQLITE3_TEXT);
      $stmt->bindValue(':first_name', $this->firstName, SQLITE3_TEXT);
      $stmt->bindValue(':last_name', $this->lastName, SQLITE3_TEXT);
      $stmt->bindValue(':theme', $this->theme, SQLITE3_TEXT);
      $stmt->bindValue(':approved', $this->approved, SQLITE3_INTEGER);
      $stmt->execute();    
    }
  }
  public function getDeleteStatement(){
    return "DELETE FROM requests WHERE id=:id";
  }
}
?>
