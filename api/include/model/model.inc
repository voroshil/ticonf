<?php

abstract class ModelObject implements JsonSerializable {
  public $id;

  function __construct($id){
    $this->id = $id;
  }
  public abstract function getDeleteStatement();

  public abstract function loadFromDb();
  public abstract function saveToDb();

  public function deleteFromDb(){
    if (!isset($this->id)){
      return;
    }
    $delete = $this->getDeleteStatement();
    $file_db = db_connect();
    $stmt = $file_db->prepare($delete);
    $stmt->bindValue(':id', $this->id, SQLITE3_INTEGER);
    $stmt->execute();    
  }

  public function jsonSerialize() {
    return get_object_vars($this);
  }
}

?>