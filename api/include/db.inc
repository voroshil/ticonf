<?php 
/*
���
�����,
�����
������
������-����� (����������� ���������� ����������)
���� �������
-------------
������������ ��������� � ��������
---------------
������ ���������� �����������
������ ���������� ������-�������

*/

 function db_connect(){
   $file_db = new PDO('sqlite:conference.sqlite3');

   $file_db->exec("CREATE TABLE IF NOT EXISTS sections (
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    name TEXT, 
                    capacity INTEGER,
                    section_type INTEGER(1) DEFAULT 0
                    )");

//   $file_db->exec("ALTER TABLE sections ADD COLUMN section_type INTEGER(1) DEFAULT 0");

   $file_db->exec("CREATE TABLE IF NOT EXISTS requests (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    section_id INTEGER,
                    email TEXT, 
                    first_name TEXT(50),
                    last_name TEXT (50),
                    theme TEXT(2000),
                    approved INTEGER(1))");
  
   $file_db->exec("CREATE TABLE IF NOT EXISTS settings (
                    key TEXT(100) PRIMARY KEY,
                    value TEXT(2000) NOT NULL)");

    return $file_db;
  }
?>
